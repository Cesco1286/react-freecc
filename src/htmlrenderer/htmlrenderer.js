import React, {useState, useEffect} from 'react';


function HtmlRenderer(props) {
    const [htmlString, updateHtmlString] = useState("Insert some HTML to render");    

    return(
        <div>
            <div>{props.input}</div>
            <input type="text" onChange= { (event) => updateHtmlString(event.target.value) }></input>
            <div dangerouslySetInnerHTML={{__html: htmlString}} />
        </div>
    );
}

export default HtmlRenderer;