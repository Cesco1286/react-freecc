import React from 'react';
import logo from './logo.svg';
import './App.css';
import HtmlRenderer from './htmlrenderer/htmlrenderer';
import Keyboard from "./drums/drums";

function App() {
  return (
    <div className="App">
      <HtmlRenderer input={"this is a prop"} />
      <Keyboard sounds={[
         {
           src : "/audio/audio1.mp3",
           id : 1234}
           ]
         } />
      </div>
  );
}

export default App;
