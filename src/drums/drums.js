import React, {useState, useEffect, useRef} from 'react';
import Key from "./key";

function Keyboard(props){    
         
    return(
    <div>
        <Key src={props.sounds[0].src} id={props.sounds[0].id} />        
    </div>
    );
}

export default Keyboard;
