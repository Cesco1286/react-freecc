import React, {useState, useEffect, useRef} from 'react';

function Key(props){
    
    const audio = useRef(null);
    const [isPlaying, switchIsPlaying] = useState(false);

    useEffect(() => {
        console.log("setting up the audio");        
    }, [])

    function play(){
        audio.current.play();
    }

    return( 
        <div>
            <div onClick={() => play() }> play audio</div>
            <audio src={props.src} id={props.id} ref={audio} />           
        </div>
    );
}

export default Key;